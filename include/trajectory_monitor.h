#ifndef TRAJECTORY_MONITOR_H
#define TRAJECTORY_MONITOR_H

#ifdef __cplusplus
extern "C"
{
#endif

int trajectory_monitor_init();
int trajectory_monitor_stop();

#ifdef __cplusplus
}//extern "C"
#endif

#endif